import model
import view
import pandas as pd
import data_cleaning as dc
import strategies
import numpy as np
import time
import os
import fake_data_generator as fdg
from calendar import monthrange
from tkinter.filedialog import askopenfilename

class Controller:

    def __init__(self, root):

        self.root = root
        self.model = model.Model()
        self.view = view.View(root)
        self.date_parsed = False
        self.boxes_updated = False
        self.view.btn_upload.config(command = self.open_file)
        self.view.btn_run_test.config(command = self.test)
        self.view.btn_update_buyHold.config(command = self.update_boxes)
        self.view.btn_fake_data.config(command = self.mock_data)
        self.days_dict = {
            'Mondays': 0,
            'Tuesdays': 1,
            'Wednesdays': 2,
            'Thursdays': 3,
            'Fridays': 4
            }

    def open_file(self):
        
        try:
            self.model.open_file()
        except IOError:
            self.view.message_label.config(text = 'Please upload .xls, .xlsx, .csv or .ods file', fg = 'red')
        
        if type(self.model.data_fr) != "NoneType":

            if self.view.test_chart_exists:
                for item in self.view.profit_chart_canvas.get_tk_widget().find_all():
                    self.view.profit_chart_canvas.get_tk_widget().delete(item)
                self.test_chart_exists = False

            if self.view.test_info_exists:
                self.view.test_data_box.configure(state='normal')
                self.view.test_data_box.delete(1.0, 'end')
                self.view.test_profit_box.configure(state='normal')
                self.view.test_profit_box.delete(1.0, 'end')
                self.view.test_data_box.configure(state='disabled')
                self.view.test_profit_box.configure(state='disabled')
                self.test_info_exists = False
                if self.view.btn_save_results["state"] == "normal":
                    self.view.btn_save_results["state"] = "disabled"

            self.date_parsed = False
            self.df = self.model.data_fr.copy()
            self.view.message_label.config(text = 'File uploaded successfully', fg = 'red')
            self.date_column = (self.df.select_dtypes(include=[np.datetime64, 
                                                      'datetime64[ns]'])).columns.values
            if len(self.date_column) == 0:
                self.view.message_label.config(text = 'No valid date column detected', fg = 'red')
                self.date_column = (self.df.select_dtypes(include=[np.object])).columns.values
            self.view.choose_date_column(self.date_column)

            self.price_column = (self.df.select_dtypes(
                include=[np.float32, np.float64, np.float16, np.float])).columns.values
            self.view.choose_price_column(self.price_column)

            if self.view.btn_update_buyHold["state"] == "disabled":
                self.view.btn_update_buyHold["state"] = "normal"
                
    def mock_data(self):

        if self.view.test_chart_exists:
            for item in self.view.profit_chart_canvas.get_tk_widget().find_all():
                self.view.profit_chart_canvas.get_tk_widget().delete(item)
            self.test_chart_exists = False

        if self.view.test_info_exists:
            self.view.test_data_box.configure(state='normal')
            self.view.test_data_box.delete(1.0, 'end')
            self.view.test_profit_box.configure(state='normal')
            self.view.test_profit_box.delete(1.0, 'end')
            self.view.test_data_box.configure(state='disabled')
            self.view.test_profit_box.configure(state='disabled')
            self.test_info_exists = False

        self.price_col = 'close'
        self.date_col = 'date'

        if self.boxes_updated == True:
            self.date_column = ['date']
            self.price_column = ['close']
            self.view.price_options_column.clear()
            self.view.date_options_column.clear()
            self.view.choose_date_column(self.date_column)
            self.view.choose_price_column(self.price_column)
            self.view.clicked_prices.set('close')
            self.view.clicked_dates.set('date')

        if self.view.btn_update_buyHold["state"] == "normal":
            self.view.btn_update_buyHold["state"] = "disabled"

        self.view.message_label.config(
            text = 'Mock data is not real and is meant only to try the app, '
                   'test results with mock data mean nothing!', fg = 'red')
        self.df = fdg.create_mock_data()

        self.view.get_df_info(self.df)
        
        self.update_buy_hold_chart(self.df)
        time.sleep(1)
        self.view.get_buy_hold_profit(self.df[self.price_col], self.df['date'])

        if type(self.model.data_fr) != 'NoneType':
            self.model.data_fr = None

        self.view.master.update()

    def check_if_df_inverted(self, df):
        last_index = df.index[-1]
        first_index = df.index[0]
        if df['date'][last_index] < df['date'][first_index]:
            df = df.iloc[::-1]
            df = df.reset_index(drop = True)
        return df
    
    def save_results(self):
        output_df = pd.DataFrame.from_dict(self.view.output, orient = 'index')
        fname = askopenfilename(filetypes=[("Excel files", "*.xlsx")])
        
        if os.path.isfile(fname):
            if fname[-4:] == 'xlsx':
                from openpyxl import load_workbook
                book = load_workbook(fname)
                writer = pd.ExcelWriter(fname, engine='openpyxl', mode = 'a', if_sheet_exists = 'overlay')
                writer.book = book
                writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
                
                write_sheet = [key for key in writer.sheets.keys()]
                
                output_df.to_excel(writer, sheet_name=write_sheet[0],
                                   startrow = writer.sheets[write_sheet[0]].max_row, header = False)
                writer.save()
                writer.close()

                self.view.message_label.config(text = 'Saved successfully to ' + fname, fg = 'red')
            else:
                self.view.message_label.config(text = 'File must be of .xlsx format' + fname, fg = 'red')

    def update_boxes(self):
        if type(self.model.data_fr) == "NoneType":
            return

        self.view.message_label.config(text='')
        self.date_col = self.view.clicked_dates.get()
        self.price_col = self.view.clicked_prices.get()
        self.df = dc.get_time_and_date(self.df, self.date_col, self.date_parsed)
        self.date_parsed = True
        self.df = self.check_if_df_inverted(self.df)
        self.view.get_df_info(self.model.data_fr)

        self.update_buy_hold_chart(self.df)
        time.sleep(1)
        self.view.get_buy_hold_profit(self.df[self.price_col], self.df['date'])
        self.view.message_label.config(text = '')

        self.boxes_updated = True
    
    def update_buy_hold_chart(self, df):
        df_profit = df.copy()
        qty = self.view.money_amt.get()/df_profit[self.price_col][0]
        df_profit['profit'] = df_profit[self.price_col] * qty
        self.view.chart_buy_and_hold(df_profit)

    def buy(self, row_price_df, buy_conditions):
        buy_amt = 0
        if self.order_size[-1] == '%':
            if 0 <= float(self.order_size[:-1]) <= 100:
                buy_amt = self.total_money * float(self.order_size[:-1]) / 100
            else:
                self.order_size = '100%'
                buy_amt = self.total_money

        else:
            if 0 <= float(self.order_size) < self.total_money:
                buy_amt = float(self.order_size)
            elif float(self.order_size) > self.total_money:
                self.view.order_size.set(self.view.money_amt.get())
                buy_amt = float(self.order_size)

        i = 0
        if len(buy_conditions) == 0 or self.bought is True:
            return

        else:
            for key, value in buy_conditions.items():
                column = value[0]

                if key == 'day':
                    if getattr(row_price_df, column) == value[1]:
                        i += 1

                elif key == 'sma':
                    if getattr(row_price_df, 'close') > getattr(row_price_df, column):
                        i += 1

                elif key == 'day_month':

                    year = pd.to_datetime(getattr(row_price_df, 'date')).year
                    month = pd.to_datetime(getattr(row_price_df, 'date')).month

                    if monthrange(year, month)[1] < column:
                        desired_date = pd.to_datetime(
                            str(year) + '-' + str(month) + '-' + str(monthrange(year, month)[1]))
                    else:
                        desired_date = pd.to_datetime(str(year) + '-' + str(month) + '-' + str(column))

                    if getattr(row_price_df, 'date') >= desired_date and np.abs(
                            getattr(row_price_df, 'date') - desired_date) < pd.Timedelta(days=5):
                        i += 1

            if i == len(buy_conditions):
                self.qty = (buy_amt - self.commission) // getattr(row_price_df, 'close')
                self.buy_amount = self.qty * getattr(row_price_df, 'close') + self.commission
                self.buy_price = getattr(row_price_df, 'close')
                self.buy_date = getattr(row_price_df, 'date')
                self.bought = True

    def sell(self, row_price_df, sell_conditions):
        i = 0
        if self.bought is True and len(sell_conditions) > 0:
            for key, value in sell_conditions.items():
                column = value[0]

                if (key == 'day') and (getattr(row_price_df, 'date') - self.buy_date) > pd.Timedelta(days=7):
                    self.bought = False
                    return

                elif (key == 'day') and (self.buy_date != getattr(row_price_df, 'date')):
                    if getattr(row_price_df, column) == value[1]:
                        i += 1
                elif key == 'sma':
                    if getattr(row_price_df, 'close') < getattr(row_price_df, column):
                        i += 1

                elif key == 'day_month':

                    year = pd.to_datetime(self.buy_date).year

                    if self.sell_day_month <= self.buy_day_month:
                        month = pd.to_datetime(self.buy_date).month + 1
                        if month == 13:
                            month = 1
                            year += 1
                    else:
                        month = pd.to_datetime(self.buy_date).month

                    if monthrange(year, month)[1] < column:
                        desired_date = pd.to_datetime(
                            str(year) + '-' + str(month) + '-' + str(monthrange(year, month)[1]))
                    else:
                        desired_date = pd.to_datetime(str(year) + '-' + str(month) + '-' + str(column))

                    if getattr(row_price_df, 'date') >= desired_date and np.abs(
                            getattr(row_price_df, 'date') - desired_date) < pd.Timedelta(days=5):
                        i += 1

            if i == len(sell_conditions):
                self.sold_condition = 'condition'
                self.sell_amount = self.qty * getattr(row_price_df, 'close') - self.commission
                self.sell_price = getattr(row_price_df, 'close')
                self.sell_date = getattr(row_price_df, 'date')
                self.bought = False
                self.pl_check = True

    def stop(self, row_price_df):

        if self.bought is True and (0 < self.stop_percent < 100):

            test_price = self.buy_price - self.buy_price * (self.stop_percent / 100)
            if getattr(row_price_df, 'close') <= test_price:
                self.sell_amount = self.qty * getattr(row_price_df, 'close') - self.commission
                self.sell_price = getattr(row_price_df, 'close')
                self.bought = False
                self.sell_date = getattr(row_price_df, 'date')
                self.pl_check = True
                self.sold_condition = 'stop'
                return True
        return False

    def take(self, row_price_df):

        if self.bought is True and (0 < self.take_percent):

            test_price = self.buy_price + self.buy_price * (self.take_percent / 100)
            if getattr(row_price_df, 'close') >= test_price:
                self.sell_amount = self.qty * getattr(row_price_df, 'close') - self.commission
                self.sell_price = getattr(row_price_df, 'close')
                self.sell_date = getattr(row_price_df, 'date')
                self.bought = False
                self.pl_check = True
                self.sold_condition = 'take'

    def profit_loss(self):
        if self.pl_check is True and self.qty > 0.0:
            self.pl = self.sell_amount - self.buy_amount
            self.total_money = self.total_money + self.pl
            if len(self.profit) == 0:
                self.profit.update({0: [self.test_df.iloc[0]['date'],
                                        self.test_df.iloc[0]['date'], 0, 0, self.money_amount]})
            self.profit.update({self.counter: [self.buy_date, self.sell_date, self.pl, self.qty,
                                               self.total_money, self.buy_price, self.sell_price,
                                               self.sold_condition]})
            self.pl_check = False
            self.counter += 1
            if self.total_money >= self.desired_amt and self.got_desired_amt == False:
                days_to_reach = self.sell_date - self.test_df['date'].iloc[0]
                years_to_reach = str(round((self.sell_date - self.test_df['date'].iloc[0]) / pd.Timedelta(days=365), 2))
                self.desired_amt_reached = years_to_reach + ' years (' +  str(days_to_reach.days) + ' days)'
                self.got_desired_amt = True

    def find_max_drawdown(self, profit, dates):
        if self.drawdown_check:
            self.past_amt = profit
            self.lowest = profit
            self.peak = profit
            self.drawdown_date = dates
            self.drawdown_check = False
            return self.max_drawdown

        else:
            self.current_amt = profit
            if self.current_amt < self.lowest:
                self.lowest = self.current_amt
            elif self.current_amt > self.peak:
                self.peak = self.current_amt
                self.lowest = self.current_amt

        if (self.max_drawdown < self.peak - self.lowest) and dates > self.drawdown_date:
            self.max_drawdown = self.peak - self.lowest
            self.drawdown_date = dates
            return self.max_drawdown

        if (self.max_drawdown < self.peak - self.lowest) and dates > self.drawdown_date:
            self.max_drawdown = self.peak - self.lowest
            self.drawdown_date = dates

    def test(self):
        self.test_df = self.df.copy()
        self.buying_strats = []
        self.selling_strats = []
        self.view.message_label.config(text='')
        self.money_amount = self.view.money_amt.get()
        self.active_strats = {self.buy_sma_test: self.view.sma_buy_check.get(),
                              self.sell_sma_test: self.view.sma_sell_check.get(),
                              self.buy_day_test: self.view.buy_day_check.get(),
                              self.sell_day_test: self.view.sell_day_check.get(),
                              self.buy_day_month_test: self.view.buy_day_month_check.get(),
                              self.sell_day_month_test: self.view.sell_day_month_check.get()
                              }

        self.commission = self.view.commission_amt.get()
        self.order_size = self.view.order_size.get()
        self.buy_day_month = self.view.buy_day_month.get()
        self.sell_day_month = self.view.sell_day_month.get()

        try:
            self.stop_percent = self.view.stop_percent.get()
        except Exception:
            self.view.stop_percent.set(0)

        try:
            self.take_percent = self.view.take_percent.get()
        except Exception:
            self.view.take_percent.set(0)

        self.desired_amt = self.view.desired_amt.get()

        self.buy_con = {}
        self.sell_con = {}

        for key, value in self.active_strats.items():
            if value == 1:
                key()

        self.run_single_test()
        self.test_results()

    def run_single_test(self):
        self.desired_amt_reached = ""
        self.got_desired_amt = False
        self.drawdown_check = True
        self.drawdown_date = pd.to_datetime('1800-01-01')
        self.peak = 0
        self.lowest = 0
        self.current_amt = 0
        self.past_amt = 0
        self.max_drawdown = 0

        self.bought = False
        self.pl_check = False
        
        self.counter = 1
        self.total_money = self.money_amount

        self.test_df['close'] = self.test_df[self.price_col]
        
        self.test_df = self.check_if_df_inverted(self.test_df)

        self.profit = {0: [self.test_df.iloc[0]['date'], self.test_df.iloc[0]['date'],
                           0, 0, self.money_amount, 0, 0, '']}
        for i in self.test_df.itertuples():
            self.buy(i, self.buy_con)
            self.take(i)
            self.stop(i)
            self.sell(i, self.sell_con)
            self.profit_loss()


        self.df_profit = pd.DataFrame.from_dict(
            self.profit, orient = 'index', columns = ['Buy date', 'Sell date', 'P/L', 'Quantity',
                                                      'Total profit', 'Buy price', 'Sell price', 'Sell reason'])


    def test_results(self):
        drawdown = map(self.find_max_drawdown, self.df_profit['Total profit'], self.df_profit['Sell date'])
        list(drawdown)
        self.view.get_test_info(self.df_profit, self.buying_strats, self.selling_strats, self.max_drawdown,
                                self.desired_amt_reached, self.view.stop_percent.get(), self.view.take_percent.get())
        self.update_buy_hold_chart(self.df)
        self.view.chart(self.df_profit)
        self.view.get_test_profit(self.view.test_profit_box, self.df_profit['Total profit'])
        if self.view.btn_save_results["state"] == "disabled":
            self.view.btn_save_results["state"] = "normal"
        self.view.btn_save_results.config(command = self.save_results)

    def buy_day_test(self):
        if self.view.clicked_buy.get() is not None:
            self.create_days_of_week(self.test_df)
            self.buy_con.update({'day': ['day', self.days_dict[self.view.clicked_buy.get()]]})
            self.buying_strats.append('Buy only on ' + self.view.clicked_buy.get())

    def sell_day_test(self):
        if self.view.clicked_sell.get() is not None:
            self.create_days_of_week(self.test_df)
            self.sell_con.update({'day': ['day', self.days_dict[self.view.clicked_sell.get()]]})
            self.selling_strats.append('Sell only on ' + self.view.clicked_sell.get())

    def buy_sma_test(self):
        
        try:
            self.view.buy_sma_length.get()
        except Exception:
            self.view.buy_sma_length.set(0)
        
        if self.view.buy_sma_length.get() > 1:
            self.test_df = self.add_sma(self.test_df[self.price_col], self.test_df, self.view.buy_sma_length.get())
            self.buy_con.update({'sma': ['sma{}'.format(self.view.buy_sma_length.get())]})
            self.buying_strats.append('Buy if price is above SMA ' + str(self.view.buy_sma_length.get()))
 
    def sell_sma_test(self):
        
        try:
            self.view.sell_sma_length.get()
        except Exception:
            self.view.sell_sma_length.set(0)
        
        if self.view.sell_sma_length.get() > 1:
            self.test_df = self.add_sma(self.test_df[self.price_col], self.test_df, self.view.sell_sma_length.get())
            self.sell_con.update({'sma': ['sma{}'.format(self.view.sell_sma_length.get())]})
            self.selling_strats.append('Sell if price is below SMA ' + str(self.view.sell_sma_length.get()))

    def buy_day_month_test(self):

        try:
            self.view.buy_day_month.get()
        except Exception:
            self.view.buy_day_month.set(0)

        if 0 < self.view.buy_day_month.get() < 32:
            self.buy_con.update({'day_month': [self.view.buy_day_month.get()]})
            self.buying_strats.append('Buy on each ' + str(self.view.buy_day_month.get()) + ' day of month')

        else:
            self.view.message_label.config(text= "Day of month must be from 1 to 31")

    def sell_day_month_test(self):

        try:
            self.view.sell_day_month.get()
        except Exception:
            self.view.sell_day_month.set(0)

        if 0 < self.view.sell_day_month.get() < 32:
            self.sell_con.update({'day_month': [self.view.sell_day_month.get()]})
            self.selling_strats.append('Sell on each ' + str(self.view.sell_day_month.get()) + ' day of month')

        else:
            self.view.message_label.config(text="Day of month must be from 1 to 31")


    def create_days_of_week(self, df):
        if 'day' not in df.columns:
            dc.get_time_and_date(df, self.date_col, self.date_parsed)

        self.date_parsed = True

    def add_sma(self, prices, df, length):

        sma = strategies.sma(length)
        sma.create_sma(prices, df)

        return df
