import random
import pandas as pd
import warnings
warnings.filterwarnings('ignore')

mock_dates = {}

start_date = pd.to_datetime('2018-01-01')


def create_mock_data():
    n = 0
    for i in range(0, 501):
        dte = start_date + pd.Timedelta(days = i)
        if dte.weekday() == 5 or dte.weekday() == 6:
            pass
        else:
            mock_dates.update({n: [dte, 0]})
            n += 1
    
    df_comp_a = pd.DataFrame.from_dict(mock_dates, orient = 'index', columns = ['date', 'close'])
    
    old_price_a = random.uniform(80, 100)
    
    for i in range(0, 359):
    
        volatility = random.uniform(0.1, 0.15)
        rnd = random.uniform(0.0, 1.0)
        change_percent = 2 * volatility * rnd
        if change_percent > volatility*1.04:
            change_percent -= (2 * volatility)     
        change_amount = old_price_a * change_percent
        new_price_a = round((old_price_a + change_amount), 2)
        old_price_a = new_price_a
        
        df_comp_a['close'][i] = new_price_a

    return df_comp_a
