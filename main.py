import warnings
import tkinter as tk
import controller
import asyncio

warnings.filterwarnings('ignore')



async def create_gui():

    if __name__ == '__main__':
        root = tk.Tk()

        app = controller.Controller(root)


        task = asyncio.create_task(launch_mainloop(root))



async def launch_mainloop(root):
    root.mainloop()


        
asyncio.run(create_gui())
