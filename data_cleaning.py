import pandas as pd
import zoneinfo
import time

            
def get_time_and_date(data_frame, date_column_name, parsed):

    if data_frame[date_column_name].dtype == 'datetime64' or data_frame[date_column_name].dtype == 'datetime64[ns]':
        data_frame['day'] = data_frame[date_column_name].dt.dayofweek
        data_frame = data_frame.rename(columns = {date_column_name: 'date'})
        return data_frame
        
    if parsed is False:

        user_time_zone = "Etc/UTC"
    
        time_zone_list = sorted(zoneinfo.available_timezones())
        time_z_split = [i.split('/') for i in time_zone_list]

        time_zone = (time.tzname[0]).split()[0]
    
        for i in time_z_split:
            if len(i) == 2:
                if time_zone == i[1]:
                    user_time_zone = ('{}/{}'.format(i[0], i[1]))
                        
            else:
                if time_zone == i[0]:
                    user_time_zone = (i[0])
        
        data_frame['date'] = 0
        first_index = data_frame[date_column_name].index[0]
        if type(data_frame[date_column_name][first_index]).__name__ == 'time':
            pass
        else:

            if(
                    data_frame[date_column_name].dtype != 'datetime64'
                    ) or (
                    data_frame[date_column_name].dtype != 'datetime64[ns]'
                    ) or (
                    data_frame[date_column_name].dtype != 'datetime.time'):
                data_frame['time'] = pd.to_datetime(data_frame[date_column_name], utc=True)
            if user_time_zone !=  'NoneType':
                data_frame['time'] = data_frame['time'].dt.tz_convert(user_time_zone)

            data_frame['date'] = data_frame['time'].dt.date
            data_frame['time'] = data_frame['time'].dt.time
            data_frame['date'] = pd.to_datetime(data_frame['date'])
            data_frame['day'] = data_frame['date'].dt.dayofweek

    return data_frame
