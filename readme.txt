Project description

The backtester application allows a user to upload spreadsheets with 
historical prices for a particular financial instruments (such as stocks, 
cryptocurrencies, futures, etc.) and test profitability of certain strategies. 

Spreadsheet formats supported for upload:
1) .xlsx;
2) .xls
3) .csv
4) .ods

After uploading the file the user will need to indicate the date 
column and price column they wish to use for the test.

After confirming the date and price columns the user wishes to use 
for the test, they will see their uploaded spreadsheet in a scrollable 
text box, the profit potential in case they simply buy the financial 
instrument and hold it starting from the first day to the last day of 
their uploaded data set, as well as the chart showing the price movement 
for the same dates.

The user can also press the "Generate mock data" button to test the 
program's functionality without uploading actual historical data.

Strategies currently implemented include:
1) buying on a certain day of the week;
2) buying when the price is above SMA (simply moving average) of 
the user-defined length;
3) selling on certain day of the week;
4) selling when the prices is below the SMA of user-defined length.

Apart from the strategies the user can also specify:
1) the starting amount of money they wish to use for the tests;
2) the broker's commission per trade;
3) stop-loss percentage (if they wish to use one);
4) take-profit percentage (if they wish to use one);
5) order size (in percent of total funds or absolute value).

After running the test, the user will see brief test results that 
include total net profit, maximum drawdown, average trade net 
profit, percent of trades profitable, etc., as well as the chart 
with the changes in their account balance in case they actually 
traded according to chosen strategies on the days provided. 
The user has the possibility to save the test results to an .xlsx file.



Technologies used

The app is written fully in Python. Libraries used include 
pandas, asyncio, tkinter, matplotlib, time, os, zoneinfo, and random.



Future implementations

The next build is expected to include:
1) additional strategies;
2) increased number of possible output file formats.



Building the project

In case you wish to use pyinstaller to build the project, 
include the following as datas:
[('controller.py', '.'),
	('controller.py', '.'),
	('view.py', '.'),
	('model.py', '.'),
	('data_cleaning.py', '.'),
	('strategies.py', '.'),
	('fake_data_generator.py', '.')]

Also make sure to include the following as hiddenimports: 
['pandas', 'tkinter', 'tkinter.ttk', 'zoneinfo', 'xlrd']
