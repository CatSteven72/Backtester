from itertools import accumulate

class sma:
    def __init__(self, length):
        self.length = length
        
    def create_sma(self, prices, df):
        
        if 'sma{}'.format(self.length) in df:
            pass
        
        else:
            df['sma{}'.format(self.length)] = 0

            for i in range(self.length-1, len(prices)):

                sma1 = accumulate(prices[i-self.length+1:i+1])
                df['sma{}'.format(self.length)][i] = list(sma1)[-1]/self.length

    def trim_sma(self, df):
        df = df.loc[df['sma{}'.format(self.length)] != 0]
        return df
