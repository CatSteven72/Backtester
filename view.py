import tkinter as tk
from tkinter import ttk
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
import datetime

class View:
    
    def __init__(self, master):

        self.master = master
        
        self.master.title("Backtester") 

        if self.master.winfo_screenwidth() < 1100:
            self.screen_width = self.master.winfo_screenwidth()
        else:
            self.screen_width = 1100
            
        if self.master.winfo_screenheight() < 800:
            self.screen_height = self.master.winfo_screenheight()
        else:
            self.screen_height = 800

        self.master.geometry(str(self.screen_width) + 'x' + str(self.screen_height)) 
        
        self.master.columnconfigure(0, weight=1)
        self.master.rowconfigure(0, weight=1)
        
        self.master_canvas = tk.Canvas(self.master,
                                       height = self.master.winfo_height(), width = self.master.winfo_width(),
                                       scrollregion=(0, 0, 1980, 1080), bd = 0, highlightthickness=0)

        self.master_vscrollbar = ttk.Scrollbar(self.master, orient = 'vertical', command = self.master_canvas.yview)
        self.master_canvas['yscrollcommand'] = self.master_vscrollbar.set
        
        self.master_hscrollbar = ttk.Scrollbar(self.master, orient = 'horizontal', command = self.master_canvas.xview)
        self.master_canvas['xscrollcommand'] = self.master_hscrollbar.set
        
        self.master_canvas.bind_all("<MouseWheel>", self._on_mousewheel)
        
        self.frm_buttons = tk.Frame(self.master_canvas)
        self.master_canvas.create_window(0, 0, anchor='nw', window=self.frm_buttons)
        self.frm_buttons.bind("<Configure>", self.on_frame_configure)
        
        self.frm_warning = tk.Frame(self.master_canvas)
        self.frm_warning1 = self.master_canvas.create_window(650, 0, anchor='n', window=self.frm_warning)
        self.frm_warning.bind("<Configure>", self.on_frame_configure)
        
        self.frm_output = tk.Frame(self.master_canvas)
        self.frm_output1 = self.master_canvas.create_window(1075, 546, anchor='e', window=self.frm_output)
        self.frm_output.bind("<Configure>", self.on_frame_configure)

        self.frm_table = tk.Frame(self.master_canvas)
        self.frm_table1 = self.master_canvas.create_window(420, 50, anchor='n', window=self.frm_table)
        self.frm_table.bind("<Configure>", self.on_frame_configure)

        self.test_results_scroll = tk.Frame(self.master_canvas)
        self.test_results_scroll1 = self.master_canvas.create_window(
            1075, 50, anchor='ne', window=self.test_results_scroll)
        self.test_results_scroll.bind("<Configure>", self.on_frame_configure)

        self.frm_chart_buy_hold = tk.Frame(self.master_canvas)
        self.frm_chart_buy_hold1 = self.master_canvas.create_window(
            420, 360, anchor='n', window=self.frm_chart_buy_hold)
        self.frm_chart_buy_hold.bind("<Configure>", self.on_frame_configure)

        self.data_box = ttk.Treeview(self.frm_table)
        
        self.profit_box = tk.Text(self.frm_table, height = 2, width = 45)
        self.profit_box.configure(state="disabled", font=("Times New Roman", 13))
        self.profit_box.tag_configure('tag-center', justify='center')

        self.test_data_box = tk.Text(self.test_results_scroll, height = 12, width = 45)
        self.test_data_box.configure(state="disabled", font=("Times New Roman", 14))
        self.test_data_box.tag_configure('tag-center', justify='center')

        self.test_scrollbar = ttk.Scrollbar(
            self.test_results_scroll, orient='vertical', command=self.test_data_box.yview)

        self.test_profit_box = tk.Text(self.test_results_scroll, height = 1, width = 45)
        self.test_profit_box.configure(state="disabled", font=("Times New Roman", 13))
        self.test_profit_box.tag_configure('tag-center', justify='center')
        
        self.btn_upload = tk.Button(
            self.frm_buttons,
            height = 3,
            width = 11,
            fg='white', 
            bg='#629460',
            text="Upload file")
        
        self.btn_fake_data = tk.Button(
            self.frm_buttons,
            height = 3,
            width = 11,
            fg='white', 
            bg='#629460',
            text = "Generate \nmock data")

        self.money_amt = tk.DoubleVar(value = 10000)
        
        self.money_label = tk.Label(self.frm_buttons, text = 'Enter test money amount')
        
        self.money_entry = tk.Entry(
            self.frm_buttons, 
            textvariable = self.money_amt
            )
        
        self.commission_amt = tk.DoubleVar(value = 1)
        self.commission_label = tk.Label(self.frm_buttons, text = 'Commission per trade')
        self.commission_entry = tk.Entry(
            self.frm_buttons,
            textvariable = self.commission_amt
            )
        
        self.stop_percent = tk.DoubleVar(value = 0)
        self.stop_label = tk.Label(self.frm_buttons, text = 'Stop loss in % below buy price \n(e. g. 10 = 10%)')
        self.stop_entry = tk.Entry(
            self.frm_buttons, 
            textvariable = self.stop_percent
            )
        
        self.take_percent = tk.DoubleVar(value = 0)
        self.take_profit_label = tk.Label(self.frm_buttons,
                                          text = 'Take profit in % above buy price \n(e. g. 10 = 10%)')
        self.take_profit_entry = tk.Entry(
            self.frm_buttons,
            textvariable = self.take_percent
            )

        self.order_size = tk.StringVar(value = '100%')
        self.order_size_label = tk.Label(self.frm_buttons,
                                         text = 'Order size in fixed amount\nor % of funds (add % sign)')
        self.order_size_entry = tk.Entry(
            self.frm_buttons,
            textvariable = self.order_size
            )

        self.btn_run_test = tk.Button(
            self.frm_buttons,
            text="Run test",
            fg='white', 
            bg='#1f74db'
        )

        self.buy_strat_label = tk.Label(self.frm_buttons, text = 'Choose when to buy')

        self.buy_day_check = tk.IntVar()
        self.sell_day_check = tk.IntVar()
        self.sma_check = tk.IntVar()
        self.sma_buy_check = tk.IntVar()
        self.sma_sell_check = tk.IntVar()
        self.buy_day_month_check = tk.IntVar()
        self.sell_day_month_check = tk.IntVar()
        
        self.buy_strat1_check = tk.Checkbutton(self.frm_buttons, text = 'Buy on specific day of week',
                                               variable = self.buy_day_check,
                                               command = self.create_buy_day_of_week_strat)

        self.buy_strat2_check = tk.Checkbutton(self.frm_buttons, text = 'Buy when price is above SMA',
                                               variable = self.sma_buy_check, command = self.create_buy_sma_strat)

        self.buy_strat3_check = tk.Checkbutton(self.frm_buttons, text = 'Buy on specific day of month',
                                               variable = self.buy_day_month_check,
                                               command = self.create_buy_day_of_month_strat)

        self.sell_strat_label = tk.Label(self.frm_buttons, text = 'Choose when to sell')

        self.sell_strat1_check = tk.Checkbutton(self.frm_buttons, text = 'Sell on specific day of week',
                                                variable = self.sell_day_check,
                                                command = self.create_sell_day_of_week_strat)

        self.sell_strat2_check = tk.Checkbutton(self.frm_buttons, text = 'Sell when price is below SMA',
                                                variable = self.sma_sell_check, command = self.create_sell_sma_strat)

        self.sell_strat3_check = tk.Checkbutton(self.frm_buttons, text='Sell on specific day of month',
                                                variable=self.sell_day_month_check,
                                                command=self.create_sell_day_of_month_strat)

        self.warning_label = tk.Label(self.frm_warning,
                                      text = 'Past performance is no guarantee of future results!', font = "Arial, 14")

        
        self.message_label = tk.Label(self.frm_warning, font = "Arial, 13")
        
        self.buy_day_exists = 0
        self.sell_day_exists = 0
        
        self.buy_sma_exists = 0
        self.buy_sma_length = tk.IntVar()
        self.buy_sma_length.set(0)

        self.buy_day_month_exists = 0
        self.buy_day_month = tk.IntVar()
        self.buy_day_month.set(0)

        self.sell_day_month_exists = 0
        self.sell_day_month = tk.IntVar()
        self.sell_day_month.set(0)
        
        self.sell_sma_exists = 0
        self.sell_sma_length = tk.IntVar()
        self.sell_sma_length.set(0)
        
        self.options_days = [
            "Mondays",
            "Tuesdays",
            "Wednesdays",
            "Thursdays",
            "Fridays"
        ]
        
        self.df_columns = {}
        
        self.btn_update_buyHold = tk.Button(
            self.frm_buttons,
            text="Apply columns",
            fg='white', 
            bg='#629460'
        )
        
        self.master_canvas.config(scrollregion=self.master_canvas.bbox("all"))
        
        self.bh_chart = False
        
        self.clicked_dates = tk.StringVar()
        self.clicked_prices = tk.StringVar()

        self.test_chart_exists = False
        self.test_info_exists = False

        self.master_widgets = {self.master_canvas: (0, 0, 1, 'news', 0, 0),
                               self.master_vscrollbar: (0, 1, 1, 'nse', 0, 0),
                               self.master_hscrollbar: (1, 0, 1, 'swe', 0, 0)}
        self.add_to_grid(self.master_widgets)

        self.frm_warning_widgets = {self.warning_label: (0, 0, 1, 'n', 0, 0),
                                    self.message_label: (1, 0, 1, 'n', 0, 0)}
        self.add_to_grid(self.frm_warning_widgets)

        self.desired_amt = tk.IntVar(value = 0)

        self.desired_amt_entry = tk.Entry(
            self.frm_buttons,
            textvariable=self.desired_amt
        )
        self.desired_amt_label = tk.Label(self.frm_buttons, text = 'Get time to reach\ndesired account balance')

        self.frm_buttons_widgets = {self.btn_upload: (0, 0, 1, 'n', 0, 0),
                                    self.btn_fake_data: (0, 1, 1, 'n', 0, 0),
                                    self.btn_update_buyHold: (4, 0, 2, "new", 5, 5),
                                    self.money_label: (5, 0, 2, "new", 5, 3),
                                    self.money_entry: (6, 0, 2, "new", 5, 5),
                                    self.commission_label: (7, 0, 2, "new", 5, 3),
                                    self.commission_entry: (8, 0, 2, "new", 5, 5),
                                    self.stop_label: (9, 0, 2, "new", 5, 3),
                                    self.stop_entry: (10, 0, 2, "new", 5, 5),
                                    self.take_profit_label: (11, 0, 2, "new", 5, 3),
                                    self.take_profit_entry: (12, 0, 2, "new", 5, 5),
                                    self.order_size_label: (14, 0, 2, "new", 5, 3),
                                    self.order_size_entry: (15, 0, 2, "new", 5, 5),
                                    self.desired_amt_label: (16, 0, 2, 'new', 0, 0),
                                    self.desired_amt_entry: (17, 0, 2, 'new', 5, 5),
                                    self.btn_run_test: (18, 0, 2, "new", 5, 5),
                                    self.buy_strat_label: (19, 0, 2, '', 0, 0),
                                    self.buy_strat1_check: (20, 0, 2, '', 0, 0),
                                    self.buy_strat2_check: (22, 0, 2, '', 0, 0),
                                    self.buy_strat3_check: (25, 0, 2, '', 0, 0),
                                    self.sell_strat_label: (28, 0, 2, '', 0, 0),
                                    self.sell_strat1_check: (29, 0, 2, '', 0, 0),
                                    self.sell_strat2_check: (31, 0, 2, '', 0, 0),
                                    self.sell_strat3_check: (34, 0, 2, '', 0, 0),

                                    }

        self.frm_table_widgets = {self.data_box: (0, 0, 1, 'nsew', 0, 4),
                                  self.profit_box: (2, 0, 1, 'new', 0, 0)}

        self.test_results_scroll_widgets = {self.test_data_box: (0, 0, 1, 'new', 0, 0),
                                            self.test_scrollbar: (0, 1, 1, 'nse', 0, 0),
                                            self.test_profit_box: (1, 0, 1, 'new', 0, 0)}

        self.add_to_grid(self.frm_buttons_widgets)
        self.add_to_grid(self.frm_table_widgets)
        self.add_to_grid(self.test_results_scroll_widgets)

        self.master_canvas.update()
    
    def _on_mousewheel(self, event):
        self.master_canvas.yview_scroll(int(-1*(event.delta/120)), "units")
    
    def on_frame_configure(self, event):
        self.master_canvas.configure(scrollregion=self.master_canvas.bbox("all"))
    
    def choose_date_column(self, col):
        if len(col) == 0:
            pass
        else:


            self.date_options_column = []
            for i in col:
                self.date_options_column.append(i)

            self.clicked_dates.set("Pick date column")
            
            self.dates_drop = tk.OptionMenu(
                self.frm_buttons,
                self.clicked_dates,
                
                *self.date_options_column
            )
            self.dates_drop.config(fg='white', bg='#629460')
            if self.dates_drop in self.frm_buttons_widgets:
                del self.frm_buttons_widgets[self.dates_drop]
            self.frm_buttons_widgets.update({self.dates_drop: (2, 0, 2, "new", 5, 5)})
            self.add_to_grid(self.frm_buttons_widgets)
            
    def choose_price_column(self, col):
        if len(col) == 0:
            pass
        else:

            self.price_options_column = []
            for i in col:
                self.price_options_column.append(i)
            
            self.clicked_prices = tk.StringVar()
            self.clicked_prices.set("Pick price column")
            
            self.price_drop = tk.OptionMenu(
                self.frm_buttons,
                self.clicked_prices,
                *self.price_options_column
            )
            self.price_drop.config(fg='white', bg='#629460')
            if self.price_drop in self.frm_buttons_widgets:
                del self.frm_buttons_widgets[self.price_drop]
            self.frm_buttons_widgets.update({self.price_drop: (3, 0, 2, "new", 5, 5)})
            self.add_to_grid(self.frm_buttons_widgets)

    def create_buy_day_of_month_strat(self):
        if self.buy_day_month_check.get() == 1 and self.buy_day_month_exists == 0:

            self.buy_day_month_label = tk.Label(self.frm_buttons,
                                                text='(if day falls on weekend\n'
                                                     'will buy on nearest day)')

            self.buy_day_month_entry = tk.Entry(
                self.frm_buttons,
                textvariable=self.buy_day_month)
            self.frm_buttons_widgets.update({self.buy_day_month_label: (26, 0, 2, "new", 5, 3)})
            self.frm_buttons_widgets.update({self.buy_day_month_entry: (27, 0, 2, "new", 5, 5)})
            self.add_to_grid(self.frm_buttons_widgets)

            self.buy_day_month_exists = 1
            self.buy_day_month.set(0)
            self.buy_day_month_exists = 1

        elif self.buy_day_month_check.get() == 0 and self.buy_day_month_exists == 1:

            if self.buy_day_month_entry in self.frm_buttons_widgets:
                del self.frm_buttons_widgets[self.buy_day_month_label]
                del self.frm_buttons_widgets[self.buy_day_month_entry]

            self.buy_day_month.set(0)
            self.buy_day_month_label.grid_forget()
            self.buy_day_month_entry.grid_forget()

            self.buy_day_month_exists = 0

    def create_sell_day_of_month_strat(self):
        if self.sell_day_month_check.get() == 1 and self.sell_day_month_exists == 0:

            self.sell_day_month_label = tk.Label(self.frm_buttons,
                                                text='(if day falls on weekend\n'
                                                     'will sell on nearest day)')

            self.sell_day_month_entry = tk.Entry(
                self.frm_buttons,
                textvariable=self.sell_day_month)

            self.frm_buttons_widgets.update({self.sell_day_month_label: (35, 0, 2, "new", 5, 3)})
            self.frm_buttons_widgets.update({self.sell_day_month_entry: (36, 0, 2, "new", 5, 5)})
            self.add_to_grid(self.frm_buttons_widgets)
            self.sell_day_month_exists = 1
            self.sell_day_month.set(0)
            self.sell_day_month_exists = 1

        elif self.sell_day_month_check.get() == 0 and self.sell_day_month_exists == 1:

            if self.sell_day_month_entry in self.frm_buttons_widgets:
                del self.frm_buttons_widgets[self.sell_day_month_label]
                del self.frm_buttons_widgets[self.sell_day_month_entry]

            self.sell_day_month.set(0)
            self.sell_day_month_label.grid_forget()
            self.sell_day_month_entry.grid_forget()

            self.sell_day_month_exists = 0

    def create_buy_day_of_week_strat(self):
        if self.buy_day_check.get() == 1 and self.buy_day_exists == 0:
            self.clicked_buy = tk.StringVar()
            self.clicked_buy.set("Pick buy day")

            self.drop_buy_day = tk.OptionMenu(
                self.frm_buttons,
                self.clicked_buy,
                *self.options_days
            )
            self.frm_buttons_widgets.update({self.drop_buy_day: (21, 0, 2, "new", 5, 5)})
            self.add_to_grid(self.frm_buttons_widgets)

            self.drop_buy_day.config(width=15)
    
            self.buy_day_exists = 1
            
        elif self.buy_day_check.get() == 0 and self.buy_day_exists == 1:

            if self.drop_buy_day in self.frm_buttons_widgets:
                del self.frm_buttons_widgets[self.drop_buy_day]

            self.clicked_buy = 0
            self.drop_buy_day.grid_forget()
            self.buy_day_exists = 0
    
    def create_sell_day_of_week_strat(self):
        if self.sell_day_check.get() == 1 and self.sell_day_exists == 0:
        
            self.clicked_sell = tk.StringVar()
            self.clicked_sell.set("Pick sell day")
            
            self.drop_sell_day = tk.OptionMenu(
                self.frm_buttons,
                self.clicked_sell,
                *self.options_days
                
            )
            self.frm_buttons_widgets.update({self.drop_sell_day: (30, 0, 2, "new", 5, 5)})
            self.add_to_grid(self.frm_buttons_widgets)

            self.drop_sell_day.config(width=15)
            self.sell_day_exists = 1
            
        elif self.sell_day_check.get() == 0 and self.sell_day_exists == 1:

            if self.drop_sell_day in self.frm_buttons_widgets:
                del self.frm_buttons_widgets[self.drop_sell_day]

            self.clicked_sell = 0
            self.drop_sell_day.grid_forget()
            self.sell_day_exists = 0
    
    def create_buy_sma_strat(self):
        if self.sma_buy_check.get() == 1 and self.buy_sma_exists == 0:
            self.buy_sma_label = tk.Label(self.frm_buttons, text = 'SMA length')

            self.buy_sma_entry = tk.Entry(
                self.frm_buttons,
                textvariable = self.buy_sma_length)
            self.frm_buttons_widgets.update({self.buy_sma_label: (23, 0, 2, "new", 5, 3)})
            self.frm_buttons_widgets.update({self.buy_sma_entry: (24, 0, 2, "new", 5, 5)})
            self.add_to_grid(self.frm_buttons_widgets)

            self.buy_sma_exists = 1
            self.buy_sma_length.set(0)
            
        elif self.sma_buy_check.get() == 0 and self.buy_sma_exists == 1:

            if self.buy_sma_entry in self.frm_buttons_widgets:
                del self.frm_buttons_widgets[self.buy_sma_label]
                del self.frm_buttons_widgets[self.buy_sma_entry]

            self.buy_sma_length.set(0)
            self.buy_sma_label.grid_forget()
            self.buy_sma_entry.grid_forget()
            self.buy_sma_exists = 0
            
    def create_sell_sma_strat(self):
        if self.sma_sell_check.get() == 1 and self.sell_sma_exists == 0:
            self.sell_sma_label = tk.Label(self.frm_buttons, text = 'SMA length')

            self.sell_sma_entry = tk.Entry(
                self.frm_buttons,
                textvariable = self.sell_sma_length)
            self.frm_buttons_widgets.update({self.sell_sma_label: (32, 0, 2, "new", 5, 3)})
            self.frm_buttons_widgets.update({self.sell_sma_entry: (33, 0, 2, "new", 5, 5)})
            self.add_to_grid(self.frm_buttons_widgets)

            self.sell_sma_exists = 1
            self.sell_sma_length.set(0)
            
        elif self.sma_sell_check.get() == 0 and self.sell_sma_exists == 1:

            if self.sell_sma_entry in self.frm_buttons_widgets:
                del self.frm_buttons_widgets[self.sell_sma_label]
                del self.frm_buttons_widgets[self.sell_sma_entry]

            self.sell_sma_length.set(0)
            self.sell_sma_label.grid_forget()
            self.sell_sma_entry.grid_forget()
            self.sell_sma_exists = 0
    
    def get_df_info(self, df):
        
        self.data_box.destroy()
        if self.data_box in self.frm_table_widgets:
            del self.frm_table_widgets[self.data_box]
        self.data_box = ttk.Treeview(self.frm_table, columns = tuple(df.columns.values), show = 'headings')
        
        for i in df.columns.values:
            self.data_box.heading(i, text = i)
            self.data_box.column(i, minwidth=75, width=0, stretch = True)
        
        for i in range(len(df)): 
            self.data_box.insert('', tk.END, values = df.iloc[i].values.flatten().tolist())
        
        self.table_yscrollbar = ttk.Scrollbar(self.frm_table, orient='vertical', command=self.data_box.yview)

        self.table_xscrollbar = ttk.Scrollbar(self.frm_table, orient='horizontal', command=self.data_box.xview)

        self.data_box['yscrollcommand'] = self.table_yscrollbar.set
        self.data_box['xscrollcommand'] = self.table_xscrollbar.set

        self.frm_table_widgets.update({self.data_box: (0, 0, 1, 'nsew', 0, 4),
                                       self.table_yscrollbar: (0, 1, 1, 'nse', 0, 0),
                                       self.table_xscrollbar: (1, 0, 1, 'sew', 0, 0)})
        self.add_to_grid(self.frm_table_widgets)
            
    def get_test_info(self, df, buying_strats, selling_strats, drawdown, got_desired_amt, stop, take):

        if len(df) != 0:
            self.test_data_box.configure(state="normal")
            self.test_data_box.delete(1.0, 'end')
            
            last_indx = df.index[-1]
            first_indx = df.index[0]
            
            total_trades = len(df)-1
            total_net_profit = round(df['P/L'].sum(), 2)
            gross_profit = round(df['P/L'].loc[df['P/L'] > 0].sum(), 2)
            gross_loss = round(df['P/L'].loc[df['P/L'] < 0].sum(), 2)
            profit_factor = abs(round(gross_profit/gross_loss, 2))
            max_drawdown = round(drawdown, 2)
            
            winning_trades = df['P/L'].loc[df['P/L'] >= 0].count()
            losing_trades = df['P/L'].loc[df['P/L'] < 0].count()
            percent_profitable = round(((winning_trades/total_trades)*100), 2)
            avg_trade_profit = round(total_net_profit/total_trades, 2)
            avg_winning_trade = round(df['P/L'].loc[df['P/L'] >= 0].mean(), 2)
            avg_losing_trade = round(df['P/L'].loc[df['P/L'] < 0].mean(), 2)
            trading_period = (df['Sell date'][last_indx] - df['Buy date'][first_indx]).days
            buying_strats = (*buying_strats,)
            selling_strats = (*selling_strats,)

            stop = str(stop) +'%'

            take = str(take) + '%'

            if got_desired_amt == '':
                got_desired_amt = "didn't reach"
            elif self.desired_amt.get() == 0:
                got_desired_amt = "none"

            self.output = {
                '': '',
                'Time of test: ': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'Buying strategies: ': ', '.join(buying_strats),
                'Selling strategies: ': ', '.join(selling_strats),
                'Total net profit: ': total_net_profit,
                'Gross profit: ': gross_profit,
                'Gross loss: ': gross_loss,
                'Profit factor: ': profit_factor,
                'Maximum drawdown: ': max_drawdown,
                'Total trades: ': total_trades,
                'Percent profitable: ': percent_profitable,
                'Winning trades: ': winning_trades,
                'Losing trades: ': losing_trades,
                'Average trade net profit: ': avg_trade_profit,
                'Average winning trade: ': avg_winning_trade,
                'Average losing trade: ': avg_losing_trade,
                'Trading period: ': str(trading_period) + ' days',
                'Commission paid: ': 2*self.commission_amt.get()*total_trades,
                'Starting capital: ': self.money_amt.get(),
                'Order size: ': self.order_size.get(),
                'Stop loss: ': stop,
                'Take profit: ': take,
                'Time to reach desired amount (' + str(self.desired_amt.get()) + '): ': got_desired_amt
                }
                
            for key, value in self.output.items():
                if key == '':
                    pass
                else:
                    self.test_data_box.insert(tk.END, key + str(value) + '\n')

            self.test_data_box.configure(state="disabled")
            self.test_data_box['yscrollcommand'] = self.test_scrollbar.set
            self.test_info_exists = True
            
    def get_test_profit(self, box, price):
        last_indx = price.index[-1]
        first_indx = price.index[0]
        
        box.configure(state = 'normal')
        box.delete(1.0, 'end')
        box.insert(
            tk.END,
            'Profit potential: ' + 
            str(round((price[last_indx]/price[first_indx] - 1)*100, 2)) + '%',
            'tag-center'
            )
        box.configure(state = 'disabled')

        return round((price[last_indx] / price[first_indx] - 1) * 100, 2)
            
    def get_buy_hold_profit(self, price, date):
        
        last_indx = price.index[-1]
        first_indx = price.index[0]
        
        qty = (self.money_amt.get() - self.commission_amt.get())//price[first_indx]
        initial_value = qty*price[first_indx] + self.commission_amt.get()
        final_value = qty*price[last_indx] - self.commission_amt.get()
        
        date = pd.to_datetime(date)
        
        self.profit_box.configure(state = 'normal')
        self.profit_box.delete(1.0, 'end')
        self.profit_box.insert(
            tk.END, 
            'Buy and hold profit potential (' + 
            str((date[last_indx] - date[first_indx]).days) +
            ' days): \n' +
            str(round((final_value/initial_value - 1)*100, 2)) + '%',
            'tag-center'
            )
        self.profit_box.configure(state = 'disabled')
    
    def chart_buy_and_hold(self, df):
        
        if self.bh_chart:
            self.buy_hold_canvas.get_tk_widget().destroy()
            self.frm_chart_buy_hold_widgets = {}

        x = df['date']
        y = df['profit']
        fig = plt.figure(constrained_layout=True)
        fig.set_dpi(67)
        plt.plot(x, y)
        plt.xlabel('time')
        plt.ylabel('total account value')
        plt.xticks(rotation=90)
        
        self.buy_hold_canvas = FigureCanvasTkAgg(fig, master=self.frm_chart_buy_hold)
        self.buy_hold_toolbar = NavigationToolbar2Tk(self.buy_hold_canvas, self.frm_chart_buy_hold, pack_toolbar=False)

        self.frm_chart_buy_hold_widgets = {self.buy_hold_canvas.get_tk_widget(): (0, 0, 1, 'n', 0, 5),
                                           self.buy_hold_toolbar: (1, 0, 1, 'n', 0, 0)}
        self.add_to_grid(self.frm_chart_buy_hold_widgets)
        
        self.bh_chart = True
        
    def chart(self, df_profit):
        x = df_profit['Buy date']
        y = df_profit['Total profit']
        fig = plt.figure(constrained_layout=True)
        fig.set_dpi(67)
        plt.plot(x, y)
        plt.xlabel('date')
        plt.ylabel('total account value')
        plt.xticks(rotation=90)

        self.btn_save_results = tk.Button(
            self.test_results_scroll,
            text="Save test results to .xlsx file"
        )
        self.profit_chart_canvas = FigureCanvasTkAgg(fig, master=self.frm_output)
        self.profit_chart_toolbar = NavigationToolbar2Tk(self.profit_chart_canvas, self.frm_output, pack_toolbar=False)

        self.frm_output_widgets = {}

        self.frm_output_widgets = {self.btn_save_results: (2, 0, 1, 'new', 0, 0),
                                   self.profit_chart_canvas.get_tk_widget(): (2, 2, 1, 'n', 0, 5),
                                   self.profit_chart_toolbar: (3, 2, 1, 'n', 0, 0),
                                   }
        self.add_to_grid(self.frm_output_widgets)

        self.test_chart_exists = True

    def add_to_grid(self, widget_list):
        for key, value in widget_list.items():
            key.grid(row = value[0], column = value[1], columnspan = value[2], sticky = value[3], padx = value[4], pady = value[5])

