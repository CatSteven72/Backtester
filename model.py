import pandas as pd
from tkinter.filedialog import askopenfilename

class Model:
    
    def __init__(self, data_fr = None):
        self.data_fr = data_fr
    
    def open_file(self):
        try:
            filepath = askopenfilename(filetypes=[("Excel files", "*.xls *.xlsx *.ods *.csv")])

        except FileNotFoundError:
            
            return
            
        else:
            if filepath[-3:] == 'csv':
                self.data_fr = pd.read_csv(filepath)
                self.data_fr = self.data_fr.dropna()
            else:
                self.data_fr = pd.read_excel(filepath)
                self.data_fr = self.data_fr.dropna()